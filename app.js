angular.module('nbBookingUi', ['ui.bootstrap','ui.router','ngAnimate']);

angular.module('nbBookingUi')
    .provider('bookingState', function ($stateProvider, BOOKING_STATES) {
        //var states={};
        this.setState=function(rootState){
            angular.forEach(BOOKING_STATES, function(value, key){
                if(rootState){
                    $stateProvider.state(rootState+'.'+key, value);
                }else{
                    $stateProvider.state(key, value);
                }
            });

        };

        this.$get = function () {

        };
    });

angular.module('nbBookingUi').constant('BOOKING_STATES', {
    booking : {
        url : '/booking-constants',
        template : 'in booking through constants'
    }
});
angular.module('nbBookingUi').config(function($stateProvider, $urlRouterProvider, bookingStateProvider) {

    /*To run the application locally, we need to un-comment the below line and comment all the states being set otherwise*/

    bookingStateProvider.setState('');

    /*To run locally, comment out all the states being set below */
    bookingStateProvider.setState('dashboard');
    bookingStateProvider.setState('apphome');
    /*Comment the states above to run the booking application locally*/

    /* Add New States Above */
    $urlRouterProvider.otherwise('/booking-constants');

});

angular.module('nbBookingUi').run(function($rootScope, $state) {

    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
console.log('listing states',$state.get());
});
